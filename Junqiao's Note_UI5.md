﻿# Junqiao's Note_UI5

---
1.	How to **set a model**.
` var oBtn = AbstractAction.prototype.getButton.apply(this, oEvent);
oBtn.setModel(sap.ui.getCore().getMessageManager().getMessageModel(), "messageModel");`
`sap.ui.getCore().getMessageManager().getMessageModel()` is to create the model (in this example, it is a message model.)
In this example, oBtn is a control whose catagory is Button.
For more specific information, see [here](https://sapui5.hana.ondemand.com/#/api/sap.ui.core.message.MessageManager/methods/getMessageModel).
2.	How to **bind property**.
```
oBtn.bindProperty("enabled", {
      parts: [{
       path: "editorModel>/hasPendingChanges"
      }, {
       path: "modifications>/modifications"
      }, {
       path: "messageModel>/"
      }],
      formatter: function(bValue, oModifications, aMessage) {
         }
});
```
“enabled” is the name of the property to be bind.
In the path, editorModel is the name of the model and hasPendingChanges is a property of this model. If you want the path to be the whole model, just use **"messageModel>/"**.
Formatter is a function which can affect the property. Three parameters bValue, oModifications and aMessage are the values of three paths correspondingly.
In this example, oBtn is a control whose catagory is Button. 
For more specific information, see [here](https://sapui5.hana.ondemand.com/#/api/sap.ui.base.ManagedObject/methods/bindProperty).
3. Want to use **iteration**?
**underscore** , this library includes a lot of functions related to iteration, written as **_.** .
For more specific information, see [here](http://www.css88.com/doc/underscore/).
4. No **Hard-coded**.
```
if (aMessage.length !== 0 && _.find(aMessage, function(oMsg) {
        if (oMsg.type === "Error"){
            return true;
        }
    })) 
```
In this example, the usage is to iterate aMessage, if its length is not 0 and there is a oMsg whose type is "Error", then returns true. However, "Error" is actually a hard-coded one. If there is an error whose name is "Error2", but still belongs to this type, then it will fail. So we should write it as 
```
if (aMessage.length !== 0 && _.find(aMessage, function(oMsg) {
						if (oMsg.type === MessageType.Error) {
							return true;
						}
					}))
```


