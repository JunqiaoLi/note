﻿# Junqiao's Note_JS

---
1.  **Apply**, **call** and **bind**
These three are used to change the pointing object in function. Bind() does not execute the function.
Example:
```javascript
var obj = {
    x: 81,
};
 
var foo = {
    getX: function() {
        return this.x;
    }
}
 
console.log(foo.getX.bind(obj)());  //81
console.log(foo.getX.call(obj));    //81
console.log(foo.getX.apply(obj));   //81
```





