﻿# Junqiao's Note_Carriers and Flights

---
Project **Flights and Carriers**
In the back-end part, create the data of Carriers, Flights and the connection between those two entities. Then in the front-end, create a two-page app. In the first page, the information of the Carriers is displayed in a table. If anyone in the table is clicked, then it will be navigated to the corresponding Flights page where the corresponding Flights information will be displayed. There should be a back button on the second page. And for different pages, the urls should be different.

1. back-end part.
Detailed tutorial is [here](http://www.bluefinsolutions.com/insights/lindsay-stanger/march-2014-(1)/building-your-first-simple-gateway-service).
  1. Create the project, remember to choose “local object”. 
  2.	Create the entity Carriers which is imported from DDIC structure.
  3.	The corresponding entity set is created automatically.
  4.	Generate and activate the service.
  5.	Add ABAP to populate the Carriers, which needs some redefinition of the CARRIERS_GET_ENTITYSET method.
  6. Then add a new entity Flights according to the above steps.
  7. The last step is to create a navigation from Carriers to Flights. 
 **In this step, I need to clear cache in FND as well as BEP (/IWBEP/cache_cleanup and   /IWFND/cache_cleanup) to make the navigation correct.**
2. front-end part.
  1. Create the home page of the app, Carrier.xml and its controller. We just need a new table in the view page. 
  **In the controller, we use `oView = this.getView()` to get the view and `oModel = new sap.ui.model.odata.ODataModel("/sap/opu/odata/sap/ZSFLIGHT_PROJECT")` to get the model. The url is found by clicking “call browser” in the NetWeaver Gateway. Usually you cannot make it in the first time, then check it in debug-Newtork where you can fink the corresponding url. Compare it with the url in the browser and modify it.
In the <Table id="idCarriersTable" items="{ path: '/Carriers'}">, we use the content in { path: '/Carriers'} to bind the data to the table. This is related to the url.
Then in the view page, we can use { } to display the binded data.**
  2. Create the Flight.xml as the second page, which also just include one table.
**We need to activate the navigation in manifest.json to make the navigation successful. 
In addition to that, we have to give the control <App id="app"> in Carrier.xml the correct id which is the same as controlId in json or there will be some bugs.
Then in Carrier.controller, we use 
`oItem = oEvent.getSource() `
`oCtx = oItem.getBindingContext()`
`this.getRouter().navTo("flight",{Carrid: oCtx.getProperty("Carrid")})`
to make url different if we click different column in the table of Carrier.**
3. **Then is the creation of Flight.controller, which is the most difficult part in this project.
In the onInit function, we need this.getRouter().getRoute("flight").attachMatched(this._onRouteMatched, this) 
to do the binding.
Then in _onRouteMatched function, we need**
```
oArgs = oEvent.getParameter("arguments")
oView = this.getView()
oTable = oView.byId("idFlightsTable")
sPath = "/Carriers('" + oArgs.Carrid + "')/Flights"
oTable.bindAggregation("items", sPath, new sap.m.ColumnListItem({cells: [
					new sap.m.Text({
						text: "{Carrid}"
					})]
			}))
```
**The oArgs.Carrid is got from the first page when you click on something. In this way, we can display the corresponding Flights information in the second page but not the whole Flights information.
In the ColumnListItem we can only have multiple text but not cells, because there can be only one cells in the ColumnListItem.
In this kind of binding, we can only add those text in control but not in view like before.**
Now the project is almost finished. However the data on the second are the same although they are different for different columns clicked in the first page.
**This is because of the wrong configuration of the key in NetWeaver Gateway. To be more detailed, the key in Flights entity includes only Carrid, which is same for all in one column in the first page. So only the last data will be displayed on the second page and repeated many times. To solve this, just add one more key in the Flights entity which is different for different data in this entity. After adding new key, do not forget to clear cache in FND as well as BEP (/IWBEP/cache_cleanup and /IWFND/cache_cleanup).**
4. Back button.
If there is some history, then use window.history.go(-1). If not, use `this.getRouter().navTo("appHome", {}, true)` to directly go back to the homepage.







